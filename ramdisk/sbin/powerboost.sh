#!/res/busybox sh

export PATH=/res/asset:$PATH

if [ ! -f /dev/bootdone ]; then
	return 1
else
	if [ "${1}" = "1" ] ; then
		if [[ $(cat /data/.arter97/powerboost) == "1" ]]; then
			/res/arter97.sh save_cpu
			echo "performance" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
			echo "$(cat /data/.arter97/powerboost_cpu_freq)" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
		fi
	fi

	if [ "${1}" = "0" ] ; then
		if [[ $(cat /data/.arter97/powerboost) == "1" ]]; then
		        rm -f /dev/bootdone
			/res/arter97.sh apply_cpu
		        touch /dev/bootdone
		fi
	fi
fi
